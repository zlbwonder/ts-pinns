
# Poisson equation in 1D with Dirichlet boundary conditions

## Problem setup
We will solve a Poisson equation:

`-\Delta u = \pi^2 \sin(\pi x), \qquad x \in [-1, 1]`

with the Dirichlet boundary conditions
```bash
         u(−1)=0,    u(1)=0.
```
The exact solution is u(x)=sin(πx).


## Requirements
Experiment environment:
```
oneflow
matplotlib
numpy
scikit-learn
scikit-optimize
scipy
```
We need to install softwares above. This library will be used for computing the evaluation metrics for intersection over union or providing fitting drawing.
```bash
pip install matplotlib
pip install scikit-optimize
pip install scipy
```

## Implemention
### Prepare data
Training and test data are obtained by sampling the boundary and domain of defined.

geometry
```
l,r = -1.0 ,1.0
```
generate train data
```
num_domain = 16 
num_boundary = 2
x_domain = np.linspace(l,r,num_domain+1,endpoint=False)[1:,None] #no boundary points
x_bd=np.array([[l],[r]])
u_domain = func(x_domain)
u_bd = func(x_bd)
x_train = np.vstack((x_domain,x_bd))
u_train = np.vstack((u_domain,u_bd))   
```
generate test data
```
num_test = 100
test_x = np.linspace(l,r,num_test+1,endpoint=False)[1:,None]
```
### Train
We need build our train net with DNN，use the equation and boundary conditions as part of the loss.

```bash
layers = [1, 50, 50, 50, 1]
model = PhysicsInformedNN(layers)
optimizer = flow.optim.Adam(model.parameters(), lr=1e-4)

```
We train the model for 10000 iterations with Adam optimizer:
```bash
model.train(iterations=10000)
```

## Util
For details, see the appendix
```python
python poisson1d_dirichlet_bd.py
```


## Appendix

```PATH
poisson_equation/poisson1d_dirichlet_bd.py
```

