# 介绍

Physics-Informed Neural Network（简称PINNS)利用了大多数物理规律都可用偏微分方程表达的特点，用简单的全连接网络初始化物理方程，通过初边值条件和方程本身的限制，得到物理方程的近似解。PINNS利用创新的AI+科学计算的方法，摒弃了传统的数值求解。本目录下提供了几个经典的PINNS方程，包括Burgers Equation、Heat Equation、Allen-Cahn Equation、Poisson Equation 1D、Poisson Equation 2D。对于每个模型，我们提供了训练代码和对应的README文件，便于开发者快速上手。

# 模型

[Burgers Equation](https://gitee.com/zhijiangtianshu/ts-pinns/tree/master/Burgers%20Equation)

[Heat Equation](https://gitee.com/zhijiangtianshu/ts-pinns/tree/master/heat%20equation)

[Allen-Cahn Equation](https://gitee.com/zhijiangtianshu/ts-pinns/tree/master/allen%20cahn)

[Poisson Equation 1D](https://gitee.com/zhijiangtianshu/ts-pinns/tree/master/poisson%20equation%201d)

[Poisson Equation 2D](https://gitee.com/zhijiangtianshu/ts-pinns/tree/master/poisson%20equation%202d)



