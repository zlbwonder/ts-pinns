# Introduction

## Heat equation

We will solve a Heat equation:
$$
\frac{\partial u}{\partial t}=\alpha \frac{\partial^2u}{\partial x^2}, \qquad x \in [-1, 1], \quad t \in [0, 1]
$$
where α=0.4 is the thermal diffusivity constant.

With Dirichlet boundary conditions:
$$
u(0,t)=u(1,t)=0,
$$
and periodic inital condition:
$$
u(x,0) = \sin (\frac{n\pi x}{L}),\qquad 0<x<L, \quad n = 1,2,.....
$$
where L=1 is the length of the bar, n=1 is the frequency of the sinusoidal initial conditions.

## Usage

### 0.Requirements

Experiment environment:

- oneflow

- matplotlib

- pyDOE

  We need to install pyDOE. The pyDOE package can provide the LHS(Latin Hypercube Sampling) method, which samples variables with uniform sampling, and then uses a random combination of these variables for one calculation of the objective function.

  ```python
  pip install pyDOE
  ```

### 1、Run Oneflow Training Script

```python
python heat.py
```

### 2、Result

Surface fitting as the following:

![](heat.png)
