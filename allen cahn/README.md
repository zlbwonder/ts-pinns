

# Introduction

## Allen-Cahn equation

We will solve a Allen-Cahn equation:
$$
\frac{\partial u}{\partial t} = d\frac{\partial^2u}{\partial x^2} + 5(u - u^3), \quad x \in [-1, 1], \quad t \in [0, 1]
$$
The boundary condition is defined as the following:
$$
u(-1, t) = u(1, t) = -1
$$
The initial condition is defined:
$$
u(x, 0) = x^2\cos(\pi x)
$$


## Usage

### 0.Requirements

Experiment environment:

- oneflow

- matplotlib

- pyDOE

  We need to install pyDOE. The pyDOE package can provide the LHS(Latin Hypercube Sampling) method, which samples variables with uniform sampling, and then uses a random combination of these variables for one calculation of the objective function.

  ```python
  pip install pyDOE
  ```

### 1、Run Oneflow Training Script

```python
python ac.py
```

### 2、Result

Surface fitting as the following:

![](./AC.png)
