
# Poisson equation over L-shaped domain

## Problem setup
We will solve a Poisson equation:

$\- u_{xx}-u_{yy} = 1, \qquad  \Omega = [-1,1]^2 \backslash [0,1]^2$

with the Dirichlet boundary conditions
```bash
         u(x,y)=0,   (x,y)∈∂Ω
```


## Requirements
Experiment environment:
```
oneflow
matplotlib
numpy
scikit-learn
scikit-optimize
scipy
```
We need to install softwares above. This library will be used for computing the evaluation metrics for intersection over union or providing fitting drawing.
```bash
pip install matplotlib
pip install scikit-optimize
pip install scipy
```

## Implemention
### Prepare data
Training and test data are obtained by sampling the boundary and domain of defined.

geometry
```
geom = Polygon([[0, 0], [1, 0], [1, -1], [-1, -1], [-1, 1], [0, 1]])
```
generate train data
```
x_train_domain = geom.random_points(1200)          # domain  points
x_train_bc = geom.random_boundary_points(120)      # boundary points     
```
generate test data
```
x_test = geom.random_points(1500)
```
### Train
We need build our train net with DNN，use the equation and boundary conditions as part of the loss.

```bash
layers = [2, 50, 50, 50, 50, 1]
model = PhysicsInformedNN(layers)
optimizer = flow.optim.Adam(model.parameters(), lr=1e-4)

```
We train the model for 50000 iterations with Adam optimizer:
```bash
model.train(iterations=50000)
```


## Appendix
For details
```bash
/poisson_equation/poisson_LShape.py
```

